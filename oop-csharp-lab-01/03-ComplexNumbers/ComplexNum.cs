﻿using System;

namespace ComplexNumbers
{
    class ComplexNum
    {
        public ComplexNum(double re, double im)
        {
            Re = re;
            Im = im;
        }

        public double Re { get; private set; }
        public double Im { get; private set; }

        public static ComplexNum operator +(ComplexNum c1,ComplexNum c2)
        {
            return new ComplexNum(c1.Re + c2.Re,c1.Im+c2.Im);
        }

        public static ComplexNum operator -(ComplexNum c1, ComplexNum c2)
        {
            return new ComplexNum(c1.Re - c2.Re, c1.Im - c2.Im);
        }

        public static ComplexNum operator *(ComplexNum c1, ComplexNum c2)
        {
            return new ComplexNum(c1.Re*c2.Re-c1.Im*c2.Im, c1.Re*c2.Im + c1.Im*c2.Re);
        }

        public static ComplexNum operator /(ComplexNum c1, ComplexNum c2)
        {
            double re = (c1.Re * c2.Re + c1.Im * c2.Im) / Math.Pow(c2.Module, 2);
            double im= (c1.Re * c2.Im + c1.Im * c2.Re) / Math.Pow(c2.Module, 2);
            return new ComplexNum(re,im);
        }


        // Restituisce il modulo del numero complesso
        public double Module
        {
            get
            {
                return Math.Sqrt(Math.Pow(this.Re, 2) + Math.Pow(this.Im, 2));
            }
        }

        // Restituisce il complesso coniugato del numero complesso (https://en.wikipedia.org/wiki/Complex_conjugate)
        public ComplexNum Conjugate
        {
            get
            {
                return new ComplexNum(this.Re, -this.Im);
            }
        }

        public ComplexNum Invert()
        {
            return this.Conjugate / new ComplexNum(Math.Pow(this.Module, 2), 0);
        }

        // Restituisce una rappresentazione idonea per il numero complesso
        public override string ToString()
        {
            String stringOut;
            if (this.Im == 0)
            {
                stringOut = this.Re.ToString();
            }
            else
            {
                if (this.Re == 0)
                {
                    stringOut = this.Im + "i";
                }
                else
                {
                    if (this.Im < 0)
                    {
                        stringOut = this.Re +""+ this.Im + "i";
                    }
                    else
                    {
                        stringOut = this.Re + "+" + this.Im + "i";
                    }
                }
            }
            return stringOut;
        }
    }
}
