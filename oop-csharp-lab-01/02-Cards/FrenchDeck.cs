﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cards
{
    public class FrenchDeck
    {
        private Card[] cards;

        public Card this[FrenchSeed s, FrenchValue v]
        {
            get { return cards[(int)s * Enum.GetNames(typeof(FrenchValue)).Length + (int)v]; }
        }
        public Card this[FrenchValue v,String color]
        {
            get {
                if(color.ToUpper()=="NERO")
                {
                    return cards[cards.Length - 2];
                }
                return cards[cards.Length-1];
            }
        }
        public FrenchDeck()
        {
        }

        public void Initialize()
        {
            int i = 0;
            cards = new Card[Enum.GetNames(typeof(FrenchSeed)).Length * Enum.GetNames(typeof(FrenchValue)).Length-2];//si sottrae 2 perchè ci sono 2 jolly e non 4
            foreach (FrenchSeed s in Enum.GetValues(typeof(FrenchSeed)))
            {
                foreach (FrenchValue v in Enum.GetValues(typeof(FrenchValue)))
                {
                    if (v == FrenchValue.JOLLY)
                    {
                        if ((int)s == 3)
                        {
                            cards[i] = new Card(v.ToString(), "NERO");
                            cards[i+1] = new Card(v.ToString(), "ROSSO");
                        }
                    }
                    else
                    {
                        cards[i] = new Card(v.ToString(), s.ToString());
                        i++;
                    }
                }
            }
        }

        public void Print()
        {
            /*
             * == README  ==
             * 
             * Questo metodo stampa tutte le carte presenti nel mazzo, con una propria rappresentazione a scelta.
             */
            foreach (Card c in cards)
            {
                Console.WriteLine(c.ToString());
            }
        }

    }

    public enum FrenchSeed
    {
        PICCHE,
        QUADRI,
        FIORI,
        CUORI
    }

    public enum FrenchValue
    {
        ASSO,
        DUE,
        TRE,
        QUATTRO,
        CINQUE,
        SEI,
        SETTE,
        OTTO,
        NOVE,
        DIECI,
        JACK,
        QUEEN,
        KING,
        JOLLY
    }
}

